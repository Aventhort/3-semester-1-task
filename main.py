import math
import my_sqrt

user_x = float(input('Enter x:'))
print(f'My sqrt implementation: {my_sqrt.my_sqrt(user_x):.8f}')
print(f'Python STDLIB Math result: {math.sqrt(user_x):.8f}')
print(f'Python sum result: {my_sqrt.my_sqrt(user_x) - math.sqrt(user_x):.1f}')
