def my_sqrt(a):
    if a == 0:
        print("Невозможно посчитать корень")
        return ValueError
    elif a < 0:
        print("Невозможно посчитать корень")
        return ValueError
    else:
        i = 1
        xn = 1
        while i <= 1000:
            fun = 1 / 2 * (xn + (a / xn))
            xn = fun
            i += 1

        return fun